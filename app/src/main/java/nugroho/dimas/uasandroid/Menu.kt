package nugroho.dimas.uasandroid

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.collab.*
import org.json.JSONArray

class Menu : AppCompatActivity(),View.OnClickListener {

    //Api
    var url ="http://192.168.43.126/Uas/show_info.php"
    val url1 ="http://192.168.43.126/Uas/show_merk.php"
    lateinit var pref : SharedPreferences
    var setwar =""

    var datamerk = ""
    var datainfo  = mutableListOf<HashMap<String,String>>()
    var daftarmerk = mutableListOf<String>()
    lateinit var AdapMerk : ArrayAdapter<String>
    lateinit var AdapterRcListHp : AdapterRcListHp
    lateinit var inputcar : EditText
    lateinit var spmerk : Spinner
    lateinit var car : Button
   // lateinit var pref : SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?)  {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.collab)

        AdapterRcListHp = AdapterRcListHp(datainfo,this)
        rchp.layoutManager = GridLayoutManager(this,2)
        rchp.adapter = AdapterRcListHp

        //deklarasi object
        inputcar = findViewById(R.id.inptype)
        spmerk = findViewById(R.id.lsmerk)
        car = findViewById(R.id.cari)


        AdapMerk = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarmerk)
        spmerk.adapter = AdapMerk
        spmerk.onItemSelectedListener = itemselect

        pref = getSharedPreferences("setting",Context.MODE_PRIVATE)
        setwar = pref.getString("warna","").toString()


        //clik
        car.setOnClickListener(this)
        crM.setOnClickListener(this)

        warna()
    }

    @SuppressLint("ResourceAsColor")
    fun  warna(){
        if(setwar=="biru"){
            bak.setBackgroundColor(Color.CYAN)
        }
        else if(setwar=="kuning"){
            bak.setBackgroundColor(R.color.colorPrimary)
        }
        else if(setwar=="hijau"){
            bak.setBackgroundColor(Color.GREEN)
        }
        else if(setwar=="putih"){
            bak.setBackgroundColor(Color.BLACK)
        }


    }



    override fun onStart() {
        super.onStart()
        Showinfo("","")
        showMerk()

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.cari->{
                Showinfo(inputcar.text.toString().trim(),"")
            }
            R.id.crM->{
                Showinfo("",datamerk)
            }
        }
    }


    // menampilkan datat
    fun Showinfo(namatype : String, namaMerk : String){
        val request = object : StringRequest(
            Request.Method.POST,url, Response.Listener { response ->
                datainfo.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var info = HashMap<String,String>()
                    info.put("nm_tp",jsonObject.getString("nama_type"))
                    info.put("nm_merk",jsonObject.getString("nama_merk"))
                    info.put("jual",jsonObject.getString("harga"))
                    info.put("url",jsonObject.getString("url"))
                    info.put("os",jsonObject.getString("os"))
                    info.put("kamera",jsonObject.getString("kamera"))
                    info.put("detail",jsonObject.getString("detail"))
                    info.put("ram",jsonObject.getString("ram"))
                    info.put("cpu",jsonObject.getString("cpu"))

                    datainfo.add(info)
                }
                AdapterRcListHp.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi",Toast.LENGTH_SHORT).show()
            }){ override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nama",namatype)
                hm.put("mek",namaMerk)
                return hm
            }
        }
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }


    fun showMerk(){
        val request  = StringRequest(Request.Method.POST,url1,
            Response.Listener { response ->
                daftarmerk.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarmerk.add(jsonObject.getString("nama_merk"))
                }
                AdapMerk.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->

            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val itemselect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spmerk.setSelection(0)
            datamerk = daftarmerk.get(0)

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            datamerk = daftarmerk.get(position)

        }

    }

}
