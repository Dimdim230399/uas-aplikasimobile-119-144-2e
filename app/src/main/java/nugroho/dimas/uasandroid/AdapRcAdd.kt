package nugroho.dimas.uasandroid

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_tambah_hp.*

class AdapRcAdd (val dataInfHp : List<HashMap<String,String>>, val tambahHp: tambahHp) : RecyclerView.Adapter<AdapRcAdd.HolderDat> () {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapRcAdd.HolderDat {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.isi_add,parent,false)
        return HolderDat(v)

    }

    override fun getItemCount(): Int {
        return dataInfHp.size
    }

    override fun onBindViewHolder(
        holder: AdapRcAdd.HolderDat,
        position: Int) {
        val data = dataInfHp.get(position)
        holder.txtip.setText(data.get("nm_tp"))
        holder.txhar.setText(data.get("jual"))
        holder.txmer.setText(data.get("nm_merk"))
        holder.Ram.setText(data.get("ram"))
        holder.Os.setText(data.get("os"))
        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.fot)


        if (position.rem(2) == 0) holder.cardview.setBackgroundColor(Color.rgb(230,245,240))
        else holder.cardview.setBackgroundColor(Color.rgb(245,245,245))

        val set1: Animation = AnimationUtils.loadAnimation(this.tambahHp, R.anim.rcanim)

        holder.cardview.startAnimation(set1)

        holder.carView.setOnClickListener{
            val posmerk = tambahHp.daftarmerk.indexOf(data.get("nm_merk"))
            val pastype = tambahHp.daftarType.indexOf(data.get("nm_tp"))
            tambahHp.sptype.setSelection(pastype)
             tambahHp.lsmerk.setSelection(posmerk)
            tambahHp.showtype("")
            tambahHp.id_info = data.get("id_info").toString()
            tambahHp.inptOs.setText(data.get("os"))
            tambahHp.inptRam.setText(data.get("ram"))
            tambahHp.inptCpu.setText(data.get("cpu"))
            tambahHp.inpdetail.setText(data.get("detail"))
            tambahHp.inptKam.setText(data.get("kamera"))
            tambahHp.inphar.setText(data.get("jual"))
            Picasso.get().load(data.get("url")).into(tambahHp.posster)
            tambahHp.datamerk = data.get("nm_merk").toString()
            tambahHp.datatype = data.get("nm_tp").toString()


            /*
              val pos = mainActivity.daftarprodi.indexOf(data.get("nama_prodi"))
              mainActivity.SpProdi.setSelection(pos)
              mainActivity.txNm.setText(data.get("nim"))
              mainActivity.txNamaMhs.setText(data.get("nama"))
              Picasso.get().load(data.get("url")).into(mainActivity.imageView2)

            var det = Intent(this.menu,detailhp::class.java)
              det.putExtra("nama",dataInfHp.get(position).get("nm_tp"))
              det.putExtra("merk",dataInfHp.get(position).get("nm_merk"))
              det.putExtra("foto",dataInfHp.get(position).get("url"))
              det.putExtra("kamera",dataInfHp.get(position).get("kamera"))
              det.putExtra("os",dataInfHp.get(position).get("os"))
              det.putExtra("harga",dataInfHp.get(position).get("jual"))
              det.putExtra("cpu",dataInfHp.get(position).get("cpu"))
              det.putExtra("detail",dataInfHp.get(position).get("detail"))
              det.putExtra("ram",dataInfHp.get(position).get("ram"))

              this.menu.startActivity(det)
          */
        }



    }



    class HolderDat(v : View) : RecyclerView.ViewHolder(v){
      /*  val txtip =  v.findViewById<TextView>(R.id.tvtipe)
        val txhar =v.findViewById<TextView>(R.id.tvsell)
        val txmer = v.findViewById<TextView>(R.id.tvmerk)
        val fot =v.findViewById<ImageView>(R.id.gam)
        val carview  = v.findViewById<CardView>(R.id.crdetail)
        */
        val txtip = v.findViewById<TextView>(R.id.tvnam)
        val txhar = v.findViewById<TextView>(R.id.tvhar)
        val txmer=v.findViewById<TextView>(R.id.tvmerk)
        val fot = v.findViewById<ImageView>(R.id.tvimg)
        val Ram =v.findViewById<TextView>(R.id.tvram)
        val Os = v.findViewById<TextView>(R.id.tvOs)
        val cardview = v.findViewById<ConstraintLayout>(R.id.lyadd)
        val carView = v.findViewById<CardView>(R.id.crdetail)
    }

}