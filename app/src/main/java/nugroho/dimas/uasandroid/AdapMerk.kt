package nugroho.dimas.uasandroid

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_merk.*

class AdapMerk(val DataMerk : List<HashMap<String,String>>,val mrk : merk) : RecyclerView.Adapter<AdapMerk.HolderMerk>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapMerk.HolderMerk {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.isi_merk,parent,false)
        return HolderMerk(v)
    }

    override fun getItemCount(): Int {
        return DataMerk.size
    }

    override fun onBindViewHolder(
        holder: AdapMerk.HolderMerk,
        position: Int) {
            var data = DataMerk.get(position)
        holder.tampil.setText(data.get("nama_merk"))

        if (position.rem(2) == 0) holder.lays.setBackgroundColor(Color.rgb(230,245,240))
        else holder.lays.setBackgroundColor(Color.rgb(255,255,245))

        val set1: Animation = AnimationUtils.loadAnimation(this.mrk, R.anim.rcanim)
        holder.lays.startAnimation(set1)

        holder.lays.setOnClickListener{
                mrk.inputmerk.setText(data.get("nama_merk"))
                mrk.id_merk = data.get("id_merk").toString()


        }

    }


    class HolderMerk(v : View) : RecyclerView.ViewHolder(v){
        val tampil = v.findViewById<TextView>(R.id.tmerk)
        val lays =v.findViewById<CardView>(R.id.crdMerk)
    }
}