package nugroho.dimas.uasandroid

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_halaman_type.*

class AdapRcTypeHp (val datatype : List<HashMap<String,String>>, val haltype : halaman_type) : RecyclerView.Adapter<AdapRcTypeHp.Holdertype>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int): AdapRcTypeHp.Holdertype {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.isi_type,parent,false)
        return Holdertype(v)
        }

    override fun getItemCount(): Int {
        return datatype.size
         }

    override fun onBindViewHolder(holder: AdapRcTypeHp.Holdertype, position: Int) {
        val  data = datatype.get(position)
        holder.isitype.setText(data.get("nama"))

        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))
        val set1: Animation = AnimationUtils.loadAnimation(this.haltype, R.anim.rcanim)

        holder.layouts.startAnimation(set1)

        holder.cartype.setOnClickListener{
            haltype.inptype.setText(data.get("nama"))
            haltype.id_typ = data.get("id").toString()
        }

      }


    class Holdertype(v :View ) : RecyclerView.ViewHolder(v){
        val isitype = v.findViewById<TextView>(R.id.tvtem)
        val layouts = v.findViewById<LinearLayout>(R.id.layoutType)
        val cartype=v.findViewById<CardView>(R.id.aksi)

    }
}