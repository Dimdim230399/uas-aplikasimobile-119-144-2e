package nugroho.dimas.uasandroid

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_halaman_type.*
import org.json.JSONArray
import org.json.JSONObject

class halaman_type : AppCompatActivity() , View.OnClickListener {
    val url2 ="http://192.168.43.126/Uas/query_type.php"
    val url1 ="http://192.168.43.126/Uas/show_merk.php"
    var url ="http://192.168.43.126/Uas/show_type.php"
    var datatype  = mutableListOf<HashMap<String,String>>()
    var daftarmerk = mutableListOf<String>()
    lateinit var AdapMerk : ArrayAdapter<String>
    lateinit var AdapRcTypeHp : AdapRcTypeHp
    lateinit var spmerk : Spinner
    var datamerk =""
    var id_typ =""
    lateinit var pref : SharedPreferences
    var setwar =""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_halaman_type)
        AdapRcTypeHp = AdapRcTypeHp(datatype,this)
        rctype.layoutManager = LinearLayoutManager(this)
        rctype.adapter = AdapRcTypeHp


        spmerk = findViewById(R.id.lsmerk)

        AdapMerk = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarmerk)
        spmerk.adapter = AdapMerk
        spmerk.onItemSelectedListener = itemselect

        pref = getSharedPreferences("setting", Context.MODE_PRIVATE)
        setwar = pref.getString("warna","").toString()

        intype.setOnClickListener(this)
        uptype.setOnClickListener(this)
        deltype.setOnClickListener(this)
        tmmerk.setOnClickListener(this)

    }

    @SuppressLint("ResourceAsColor")
    fun  warna(){
        if(setwar=="biru"){
            bak.setBackgroundColor(Color.CYAN)
        }
        else if(setwar=="kuning"){
            bak.setBackgroundColor(R.color.colorPrimary)
        }
        else if(setwar=="hijau"){
            bak.setBackgroundColor(Color.GREEN)
        }
        else if(setwar=="putih"){
            bak.setBackgroundColor(Color.BLACK)
        }


    }


    override fun onStart() {
        super.onStart()
        showMerk()
        showtype("")
    }


    fun showMerk(){
        val request  = StringRequest(
            Request.Method.POST,url1,
            Response.Listener { response ->
                daftarmerk.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarmerk.add(jsonObject.getString("nama_merk"))
                }
                AdapMerk.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->

            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }



    val itemselect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spmerk.setSelection(0)
            datamerk = daftarmerk.get(0)

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            datamerk = daftarmerk.get(position)
            showtype(datamerk)
            //Showinfo(datamerk)
        }

    }

    fun showtype(merk : String){
        val request = object : StringRequest(
            Request.Method.POST,url, Response.Listener { response ->
                datatype.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var typ = HashMap<String,String>()
                    typ.put("nama",jsonObject.getString("nama_type"))
                    typ.put("id",jsonObject.getString("id_type"))
                    datatype.add(typ)
                }
                AdapRcTypeHp.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
            }){ override fun getParams(): MutableMap<String, String> {
            val hm = HashMap<String,String>()
            hm.put("nam",merk)
            return hm
        }
        }
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)

    }

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,url2,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    showtype(datamerk)
                    inptype.setText("")
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                //var nmFile = "DC"+SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"  = > memberinama pada foto
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                       // hm.put("id_type",id_typ)
                        hm.put("nama_type",inptype.text.toString())
                        hm.put("nama_merk",datamerk)

                        //hm.put("image",imStr)
                        //hm.put("file",namafile)
                        //hm.put("nama_prodi",pilihprodi)
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("id_type",id_typ)
                        hm.put("nama_type",inptype.text.toString())
                      //  hm.put("nama",txNamaMhs.text.toString())
                       // hm.put("image",imStr)
                        //hm.put("file",namafile)
                       // hm.put("nama_prodi",pilihprodi)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("id_type",id_typ)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.intype->{
                query("insert")

            }
            R.id.uptype->{
                query("update")
            }
            R.id.deltype->{
                query("delete")

            }
            R.id.tmmerk->{
                val intent = Intent(this,merk::class.java)
                startActivity(intent)
            }
        }
    }


}
