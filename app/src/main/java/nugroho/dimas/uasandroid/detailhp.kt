package nugroho.dimas.uasandroid

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detailhp.*

class detailhp : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailhp)

        var aa = intent
        var nama = aa.getStringExtra("nama")
        var image = aa.getStringExtra("foto")
        var harga = aa.getStringExtra("harga")
        var os = aa.getStringExtra("os")
        var kamra = aa.getStringExtra("kamera")
        var detail = aa.getStringExtra("detail")
        var rm = aa.getStringExtra("ram")
        var cpu = aa.getStringExtra("cpu")
        var nma = aa.getStringExtra("merk")

        typehp.setText(nama)
        Picasso.get().load(image).into(imgtv)
        Hargahp.setText("Rp."+harga)
        deskrip.setText(detail)
        nmmerk.setText(nma)
        oshp.setText(os)
        ram.setText(rm)
        cpuhp.setText(cpu)
        kamera.setText(kamra)
        ada.setText(nama)

        beli.setOnClickListener(this)


    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.beli->{
                val bel = Intent(Intent.ACTION_VIEW)
                bel.setData(Uri.parse("http://api.whatsapp.com/send?phone="+"+62895396119349"+"&text="+"Hallo SearchYourPhone Admin@DimasSetyoN Apakah Hp dengan type"+typehp.text.toString()+ram.text.toString()+kamera.text.toString()+oshp.text.toString()+"Stok nya masih ada ? "))
                startActivity(bel)
            }
        }
    }
}
