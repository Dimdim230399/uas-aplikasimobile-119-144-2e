package nugroho.dimas.uasandroid

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.GridLayout
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_merk.*
import org.json.JSONArray
import org.json.JSONObject

class merk : AppCompatActivity(),View.OnClickListener {

    val url1 ="http://192.168.43.126/Uas/show_merk.php"
    val url2 ="http://192.168.43.126/Uas/query_merk.php"
    var id_merk = ""
    lateinit var AdapMerk :AdapMerk
    var datMerk  = mutableListOf<HashMap<String,String>>()
    lateinit var pref : SharedPreferences
    var setwar =""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_merk)
        AdapMerk = AdapMerk(datMerk,this)

        rcMerk.layoutManager = GridLayoutManager(this,2)
        rcMerk.adapter = AdapMerk

        pref = getSharedPreferences("setting", Context.MODE_PRIVATE)
        setwar = pref.getString("warna","").toString()



        insmerk.setOnClickListener(this)
        upmerk.setOnClickListener(this)
        delmerk.setOnClickListener(this)



    }

    fun  warna(){
        if(setwar=="biru"){
            bak.setBackgroundColor(Color.CYAN)
        }
        else if(setwar=="kuning"){
            bak.setBackgroundColor(R.color.colorPrimary)
        }
        else if(setwar=="hijau"){
            bak.setBackgroundColor(Color.GREEN)
        }
        else if(setwar=="putih"){
            bak.setBackgroundColor(Color.BLACK)
        }


    }


    override fun onStart() {
        super.onStart()
        showMerk()
    }

    fun showMerk(){
        val request =  StringRequest(
            Request.Method.POST,url1, Response.Listener { response ->
                datMerk.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var info = HashMap<String,String>()
                    info.put("nama_merk",jsonObject.getString("nama_merk"))
                    info.put("id_merk",jsonObject.getString("id_merk"))

                    datMerk.add(info)
                }
                AdapMerk.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
            })
            val queue =  Volley.newRequestQueue(this)
            queue.add(request)

    }

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,url2,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    showMerk()
                    inputmerk.setText("")
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                //var nmFile = "DC"+SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"  = > memberinama pada foto
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        // hm.put("id_type",id_typ)
                        hm.put("nama_merk",inputmerk.text.toString())


                        //hm.put("image",imStr)
                        //hm.put("file",namafile)
                        //hm.put("nama_prodi",pilihprodi)
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("id_merk",id_merk)
                        hm.put("nama_merk",inputmerk.text.toString())
                        //  hm.put("nama",txNamaMhs.text.toString())
                        // hm.put("image",imStr)
                        //hm.put("file",namafile)
                        // hm.put("nama_prodi",pilihprodi)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("id_merk",id_merk)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.insmerk->{
                query("insert")
            }
            R.id.upmerk->{
                query("update")
            }
            R.id.delmerk->{
                query("delete")

            }
        }
    }

}
