package nugroho.dimas.uasandroid

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.icu.text.SimpleDateFormat
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_tambah_hp.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

class tambahHp : AppCompatActivity() , View.OnClickListener{

    //lateinit var pref : SharedPreferences
    var setwar =""

    var daftarmerk = mutableListOf<String>()
    var daftarType = mutableListOf<String>()
   // var daftarType =  mutableListOf<String>()
    val url1 ="http://192.168.43.126/Uas/show_merk.php"
    var url ="http://192.168.43.126/Uas/show_type.php"
    var url2 ="http://192.168.43.126/Uas/show_info.php"
    var uri3 = "http://192.168.43.126/Uas/query_data.php"

    var datainfo  = mutableListOf<HashMap<String,String>>()
    lateinit var  AdapRcAdd : AdapRcAdd
    lateinit var AdapMerk : ArrayAdapter<String>
    lateinit var AdapRcTypeHp : ArrayAdapter<String>
    lateinit var  MediaKamera : MediaKamera
    lateinit var spmerk : Spinner
    lateinit var sptype : Spinner
    lateinit var UploadHelper : UploadHelper
    var datamerk = ""
    var datatype =""
    var imStr = ""
    var getmerk =""
    var gettype =""
    var id_info=""
    var Fileuri = Uri.parse("")
    var namafile = ""

    lateinit var pref : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_hp)
        AdapRcAdd = AdapRcAdd(datainfo,this)
        rcadd.layoutManager = LinearLayoutManager(this)
        rcadd.adapter = AdapRcAdd
        //spin1

        //spmerk = findViewById(R.id.lsmerk)
        AdapMerk = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarmerk)
        lsmerk.adapter = AdapMerk
        lsmerk.onItemSelectedListener = itemselect

        //spin2
        sptype = findViewById(R.id.lstype)
        AdapRcTypeHp = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarType)
        sptype.adapter =AdapRcTypeHp
        sptype.onItemSelectedListener = itemselect2



        //upload foto
        UploadHelper = UploadHelper(this)
        //btn
        uploadfoto.setOnClickListener(this)
        uptype.setOnClickListener(this)
        deltype.setOnClickListener(this)
        intype.setOnClickListener(this)
        potho.setOnClickListener(this)
        addmerk.setOnClickListener(this)
        addtype.setOnClickListener(this)


        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        MediaKamera = MediaKamera()


        pref = getSharedPreferences("setting",Context.MODE_PRIVATE)
        setwar = pref.getString("warna","").toString()

    }


    @SuppressLint("ResourceAsColor")
    fun  warna(){
        if(setwar=="biru"){
            bak.setBackgroundColor(Color.CYAN)
        }
        else if(setwar=="kuning"){
            bak.setBackgroundColor(R.color.colorPrimary)
        }
        else if(setwar=="hijau"){
            bak.setBackgroundColor(Color.GREEN)
        }
        else if(setwar=="putih"){
            bak.setBackgroundColor(Color.BLACK)
        }


    }


    override fun onStart() {
        super.onStart()
        showMerk()
        Showinfo("","")
        warna()
    }


    //show merk
    fun showMerk(){
        val request  = StringRequest(Request.Method.POST,url1,
            Response.Listener { response ->
                daftarmerk.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarmerk.add(jsonObject.getString("nama_merk"))
                }
                AdapMerk.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->

            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }






    //show type
    fun showtype(merk : String){
        val request = object : StringRequest(
            Request.Method.POST,url, Response.Listener { response ->
                daftarType.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarType.add(jsonObject.getString("nama_type"))
                }
                AdapRcTypeHp.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
            }){ override fun getParams(): MutableMap<String, String> {
            val hm = HashMap<String,String>()
            hm.put("nam",merk)
            return hm
        }
        }
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)

    }

    val itemselect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            lsmerk.setSelection(0)
            datamerk = daftarmerk.get(0)

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            datamerk = daftarmerk.get(position)


        }

    }
    //spType
    val itemselect2 = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            sptype.setSelection(0)
            datatype = daftarType.get(0)

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            datatype = daftarType.get(position)

        }

    }


    //show info
    fun Showinfo(namatype : String, namaMerk : String){
        val request = object : StringRequest(
            Request.Method.POST,url2, Response.Listener { response ->
                datainfo.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var info = HashMap<String,String>()
                    info.put("nm_tp",jsonObject.getString("nama_type"))
                    info.put("nm_merk",jsonObject.getString("nama_merk"))
                    info.put("jual",jsonObject.getString("harga"))
                    info.put("url",jsonObject.getString("url"))
                    info.put("os",jsonObject.getString("os"))
                    info.put("kamera",jsonObject.getString("kamera"))
                    info.put("detail",jsonObject.getString("detail"))
                    info.put("ram",jsonObject.getString("ram"))
                    info.put("cpu",jsonObject.getString("cpu"))
                    info.put("id_info",jsonObject.getString("id_info"))

                    datainfo.add(info)
                }
                AdapRcAdd.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi",Toast.LENGTH_SHORT).show()
            }){ override fun getParams(): MutableMap<String, String> {
            val hm = HashMap<String,String>()
            hm.put("nama",namatype)
            hm.put("mek",namaMerk)
            return hm
        }
        }
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }










    //method upload
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if (requestCode == MediaKamera.getRcCamera()){
               imStr = MediaKamera.getBitmapToString(posster,Fileuri)
                namafile = MediaKamera.getMyfile()
            }else if(requestCode == UploadHelper.getRcGalery()){
                 imStr = UploadHelper.getBitmapToString(data!!.data,posster)
             }
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.uploadfoto->{
                 val intent  = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,UploadHelper.getRcGalery())

            }
            R.id.addmerk->{
                getmerk = datamerk
                showtype(getmerk)
            }
            R.id.addtype->{
                getmerk = datamerk.toString()
                gettype = datatype.toString()

                pref = getSharedPreferences("kunci", Context.MODE_PRIVATE)
                val ambil = pref.edit()
                ambil.putString("type",gettype.toString())
                ambil.putString("merk",getmerk.toString())
                ambil.commit()
                Toast.makeText(this,gettype + getmerk, Toast.LENGTH_LONG).show()

            }
            R.id.intype->{
                query("insert")
            }
            R.id.uptype->{
                query("update")
            }
            R.id.deltype->{
                query("delete")
            }
            R.id.potho->{
                requestPermition()
            }
        }
    }


    //query Crud

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,uri3,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    Showinfo("","")
                    val Inr = Intent(this,Menu::class.java)
                    startActivity(Inr)

                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                 namafile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"
                when(mode){
                    "insert"->{
                        pref = getSharedPreferences("kunci",Context.MODE_PRIVATE)
                        var aa = pref.getString("type","")
                        var ab = pref.getString("merk","")
                        hm.put("mode","insert")
                        hm.put("nama_type",aa.toString())
                        hm.put("nama_merk",ab.toString())
                        hm.put("image",imStr)
                        hm.put("file",namafile)
                        hm.put("os",inptOs.text.toString())
                        hm.put("ram",inptRam.text.toString())
                        hm.put("kamera",inptKam.text.toString())
                        hm.put("cpu",inptCpu.text.toString())
                        hm.put("harga",inphar.text.toString())
                        hm.put("detail",inpdetail.text.toString())
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("nama_type",datatype)
                        hm.put("nama_merk",datamerk)
                        hm.put("image",imStr)
                        hm.put("file",namafile)
                        hm.put("os",inptOs.text.toString())
                        hm.put("ram",inptRam.text.toString())
                        hm.put("kamera",inptKam.text.toString())
                        hm.put("cpu",inptCpu.text.toString())
                        hm.put("harga",inphar.text.toString())
                        hm.put("detail",inpdetail.text.toString())
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("nama_type",id_info)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    // Media kamera permission
    fun requestPermition() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        Fileuri = MediaKamera.getOutputFileUri()

        val inten  = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        inten.putExtra(MediaStore.EXTRA_OUTPUT,Fileuri)
        startActivityForResult(inten,MediaKamera.getRcCamera())
    }


}
