package nugroho.dimas.uasandroid

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.cardview.widget.CardView

class Laman_mainmenu : AppCompatActivity(), View.OnClickListener {
    lateinit var tmdt : CardView
    lateinit var tmset :CardView
    lateinit var tmhar : CardView
    lateinit var tmdd : CardView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_laman_mainmenu)
        //deklarasi id card
        tmdt = findViewById(R.id.showhp)
        tmset = findViewById(R.id.showset)
        tmhar = findViewById(R.id.showadd)
        tmdd = findViewById(R.id.showhar)

        //set onclik
        tmdt.setOnClickListener(this)
        tmset.setOnClickListener(this)
        tmhar.setOnClickListener(this)
        tmdd.setOnClickListener(this)


    }

    //clik
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.showhp->{
                val pindah = Intent(this,Menu::class.java)
                startActivity(pindah)
            }
            R.id.showset->{
                val pindah = Intent(this,setting::class.java)
                startActivity(pindah)

            }
            R.id.showadd->{
               val pindah = Intent(this,tambahHp::class.java)
                startActivity(pindah)
            }
            R.id.showhar->{
                val pindah = Intent(this,halaman_type::class.java)
                startActivity(pindah)
            }

        }
    }
}
