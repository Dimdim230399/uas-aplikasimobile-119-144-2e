package nugroho.dimas.uasandroid

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Thread.sleep

class MainActivity : AppCompatActivity() {

    private lateinit var tv: TextView
    private lateinit var iv: ImageView
    private lateinit var ik: TextView

    lateinit var pref : SharedPreferences
    var setwar =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tv = findViewById(R.id.tv)
        iv = findViewById(R.id.iv)
        ik = findViewById(R.id.tk)
        val set1: Animation = AnimationUtils.loadAnimation(this, R.anim.rcanim)
        val set: Animation = AnimationUtils.loadAnimation(this, R.anim.mytransition)
        tv.startAnimation(set)
        iv.startAnimation(set1)
        ik.startAnimation(set)

        pref = getSharedPreferences("setting", Context.MODE_PRIVATE)
        setwar = pref.getString("warna","").toString()


        val intent =  Intent(this, Laman_mainmenu::class.java)
        /*  val handler = Handler()
          handler.postDelayed({

              startActivity(intent)
          }, 5000)
         */

        val tim: Thread
        tim = Thread {
                try {
                    sleep(5000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                } finally {
                    startActivity(intent)
                    finish()
                }
            }
        tim.start()
        warna()
    }


    @SuppressLint("ResourceAsColor")
    fun  warna(){
        if(setwar=="biru"){
            splash.setBackgroundColor(Color.CYAN)
        }
        else if(setwar=="kuning"){
            splash.setBackgroundColor(R.color.colorPrimary)
        }
        else if(setwar=="hijau"){
            splash.setBackgroundColor(Color.GREEN)
        }
        else if(setwar=="putih"){
            splash.setBackgroundColor(Color.BLACK)
        }


    }


}

