package nugroho.dimas.uasandroid

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterRcListHp(val dataInfHp : List<HashMap<String,String>>, val menu : Menu) : RecyclerView.Adapter<AdapterRcListHp.HolderDat> () {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterRcListHp.HolderDat {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.isi_dafhp,parent,false)
        return HolderDat(v)

    }

    override fun getItemCount(): Int {
        return dataInfHp.size
    }

    override fun onBindViewHolder(
        holder: AdapterRcListHp.HolderDat,
        position: Int) {
    val data = dataInfHp.get(position)
        holder.txtip.setText(data.get("nm_tp"))
        holder.txhar.setText(data.get("jual"))
        holder.txmer.setText(data.get("nm_merk"))
        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.fot)
        var kamera = data.get("kamera")
        var os  = data.get("os")
        var cpu = data.get("cpu")
        var detail = data.get("detail")
        var ram = data.get("ram")

        val set: Animation = AnimationUtils.loadAnimation(this.menu, R.anim.rcanim)
        holder.carview.startAnimation(set)



        holder.carview.setOnClickListener{
            var det = Intent(this.menu,detailhp::class.java)
            det.putExtra("nama",dataInfHp.get(position).get("nm_tp"))
            det.putExtra("merk",dataInfHp.get(position).get("nm_merk"))
            det.putExtra("foto",dataInfHp.get(position).get("url"))
            det.putExtra("kamera",dataInfHp.get(position).get("kamera"))
            det.putExtra("os",dataInfHp.get(position).get("os"))
            det.putExtra("harga",dataInfHp.get(position).get("jual"))
            det.putExtra("cpu",dataInfHp.get(position).get("cpu"))
            det.putExtra("detail",dataInfHp.get(position).get("detail"))
            det.putExtra("ram",dataInfHp.get(position).get("ram"))

            this.menu.startActivity(det)
        }


    }



    class HolderDat(v :View) : RecyclerView.ViewHolder(v){
        val txtip =  v.findViewById<TextView>(R.id.tvtipe)
        val txhar =v.findViewById<TextView>(R.id.tvsell)
        val txmer = v.findViewById<TextView>(R.id.tvmerk)
        val fot =v.findViewById<ImageView>(R.id.gam)
        val carview  = v.findViewById<CardView>(R.id.crdetail)


    }

}