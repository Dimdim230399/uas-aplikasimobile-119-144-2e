package nugroho.dimas.uasandroid

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_setting.*

class setting : AppCompatActivity(), View.OnClickListener {

    lateinit var pref : SharedPreferences
    var setwar =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        pref = getSharedPreferences("setting",Context.MODE_PRIVATE)
        setwar = pref.getString("warna","").toString()
        putih.setOnClickListener(this)
        biru.setOnClickListener(this)
        kuning.setOnClickListener(this)
        hijau.setOnClickListener(this)

    }

    override fun onStart() {
        super.onStart()
        warna()
    }

    @SuppressLint("ResourceAsColor")
    fun  warna(){
        if(setwar=="biru"){
            bak.setBackgroundColor(Color.CYAN)
        }
        else if(setwar=="kuning"){
            bak.setBackgroundColor(R.color.colorPrimary)
        }
        else if(setwar=="hijau"){
            bak.setBackgroundColor(Color.GREEN)
        }
        else if(setwar=="putih"){
            bak.setBackgroundColor(Color.BLACK)
        }


    }



    override fun onClick(v: View?) {
            when(v?.id){
                R.id.putih->{
                    pref =getSharedPreferences("setting",Context.MODE_PRIVATE)
                    val warna = pref.edit()
                    warna.putString("warna","putih")
                    warna.commit()
                  Toast.makeText(this,"Warna Background Hitam Di Pilih ",Toast.LENGTH_SHORT).show()
                    val ulang =  Intent(this,MainActivity::class.java)
                    startActivity(ulang)
                }
                R.id.biru->{
                    pref =getSharedPreferences("setting",Context.MODE_PRIVATE)
                    val warna = pref.edit()
                    warna.putString("warna","biru")
                    warna.commit()


                    Toast.makeText(this,"Warna Background Biru Di Pilih ",Toast.LENGTH_SHORT).show()
                    val ulang =  Intent(this,MainActivity::class.java)
                    startActivity(ulang)
                }
                R.id.kuning->{

                    pref =getSharedPreferences("setting",Context.MODE_PRIVATE)
                    val warna = pref.edit()
                    warna.putString("warna","kunin")
                    warna.commit()

                    Toast.makeText(this,"Warna Background Kuning Di Pilih ",Toast.LENGTH_SHORT).show()
                    val ulang =  Intent(this,MainActivity::class.java)
                    startActivity(ulang)

                }
                R.id.hijau->{
                    pref =getSharedPreferences("setting",Context.MODE_PRIVATE)
                    val warna = pref.edit()
                    warna.putString("warna","hijau")
                    warna.commit()

                    Toast.makeText(this,"Warna Background Hijau Di Pilih ",Toast.LENGTH_SHORT).show()
                    val ulang =  Intent(this,MainActivity::class.java)
                    startActivity(ulang)
                }
            }


    }
}
